# ImageOfTheHour

An opsdroid skill that sends an Image to a matrix room at a set time interval.

## Setup

Edit configuration.yaml with a matrix account's information.
Put jpeg or png files into the Photos directory.
`docker-compose up -d`

## How to change the send interval

Open Skill/__init__.py file and change the line `@match_crontab('* * * * *', timezone="UTC")`
Opsdroid uses standard cron formatting.