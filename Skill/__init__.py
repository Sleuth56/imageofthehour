# Opsdroid
from opsdroid.connector.matrix import ConnectorMatrix
from opsdroid.events import OpsdroidStarted, Message, UserInvite, JoinRoom, Image
from opsdroid.matchers import match_event, match_regex, match_crontab, match_catchall
from opsdroid.skill import Skill

import logging

_LOGGER = logging.getLogger(__name__)

import os, random

import yaml
with open('/home/opsdroid/.config/opsdroid/configuration.yaml') as f:
  global config, folder
  config = yaml.safe_load(f.read())
  folder = config['skills']['ImageOfTheHour']['photos_directory']

class ImageOfTheHour(Skill):
  def __init__(self, opsdroid, config):
    super().__init__(opsdroid, config)

  @match_crontab(config['skills']['ImageOfTheHour']['cron_run_time'], timezone=config['skills']['ImageOfTheHour']['timezone'])
  async def scanDirectory(self, event):
    _LOGGER.info(os.listdir(folder))
    file = random.choice(os.listdir(folder))
    while (await self.opsdroid.memory.get('oldFile') == file):
      file = random.choice(os.listdir(folder))
    await self.opsdroid.memory.put('oldFile', file)
    _LOGGER.info(file)
    with open(f"{folder}/{file}", "rb") as image:
      f = image.read()
      b = bytearray(f)
      for room in self.opsdroid.config['connectors']['matrix']['rooms']:
        await self.opsdroid.send(Image(bytes(b), target=room))